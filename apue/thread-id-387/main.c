#include <pthread.h>
#include <errno.h>
#include <unistd.h>

pthread_t ntid;


void printIds(const char *s){
    pid_t pid;
    pthread_t tid;

    pid = getpid();
    tid = pthread_self();

    printf("%s pid %lu tid %lu (0x%lx)\n", s, (unsigned long) pid, (unsigned long) tid);
    //Why am I doing all these castings?
    //Don't know, seems to print normally even without the casting
}

void * thr_fn(void *arg){
    printIds("New thread");
    return((void*)0);
    //What is the thing it's returning?
}

int main(int argc, char *argv[])
{
    int err;
    err = pthread_create(&ntid, NULL, thr_fn, NULL);
    //Is that function by default passed as a pointer?
    //Yup, but that's in the declaration. When passing a function name, pointer is assumed
    if (err != 0) {
            char buf[200];
            sprintf(buf, "can't create thread");
            perror(buf);
            abort();
    }
    printIds("Main thread");
    sleep(1);
    exit(0);
}
