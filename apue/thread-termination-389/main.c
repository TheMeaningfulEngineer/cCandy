#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>

void err_exit(const char* text){
    char buf[200];
    sprintf(buf, text);
    perror(buf);
    abort();
}

void * thr_fn1(void *arg)
{
    printf("thread 1 returning \n");
    return ((void *)1);
    //This is returning the pointer to 1.
    //Is this address somewhat special?
    //It is just an address which we're using to represent
    //an exit value. It will work ok if we don't dereference it :)
}

int main(int argc, char *argv[])
{
    int         err;
    pthread_t   tid1;
    void        *tret;

    err = pthread_create(&tid1, NULL, thr_fn1, NULL);
    if (err != 0) {
        err_exit("can't create thread 1");
    }

    err = pthread_join(tid1, &tret);
    if (err != 0) {
        err_exit("can't join with thread 1");
    }

    printf("Return of thread 1 %ld\n", (long)tret);
    //Why can I safely cast this to long?
    //My assumption is that we're persuming int and long are
    //the same size
    return 0;
}
